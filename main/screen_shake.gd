extends Camera2D


var current_strength := 0.0
var shaking_active:= false
var strength_low: int
var strength_high: int
var rng = RandomNumberGenerator.new()


func _ready():
	Effect.connect("screen_shake", self, "on_screen_shake")


func on_screen_shake(duration: float, strength: float):
	if strength >= current_strength:
		current_strength = strength
		shaking_active = true
		strength_low = - int(floor(strength / 2.0))
		strength_high = int(ceil(strength / 2.0))
		$Duration.start(duration)


func _process(_delta):
	if shaking_active:
		var x = strength_low if randf() > 0.5 else strength_high
		var y = strength_low if randf() > 0.5 else strength_high
		offset = Vector2(x, y)


func _on_duration_timeout():
	shaking_active = false
	current_strength = 0.0
	offset = Vector2(0, 0)
	
	
