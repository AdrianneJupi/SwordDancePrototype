extends Node2D

enum states {IDLE, SPIN}
var state : int = states.IDLE

var spin : Dictionary #spinner, center, start_vec, rotation
var spin_progress : float
var spin_180_time := 0.5 #time in seconds to spin 180°
onready var char_a : Character = $CharacterA
onready var char_b : Character = $CharacterB
onready var link_length : float = char_a.position.distance_to(char_b.position)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_orient_characters()
	_recenter_characters()
	for character in [char_a, char_b]:
		character.connect("has_spin_moved", self, "char_has_spin_moved")
		character.connect("spin_collision", self, "end_spin", [true])


func _input(event) -> void:
	if event is InputEventMouseButton and not event.pressed:
		if event.button_index == 1 or event.button_index == 2:
			var a_around_b = event.button_index == 1
			var mouse_position = get_global_mouse_position()
			_start_spin(mouse_position, a_around_b)


func _process(delta) -> void:
	pass


#SPIN

func _start_spin(mouse_position: Vector2, a_around_b: bool) -> void:
	var order = [char_a, char_b] if a_around_b else [char_b, char_a]
	spin["center"] = order[1].global_position
	spin["spinner"] = order[0]
	spin["start_vec"] = spin["spinner"].global_position - spin["center"]
	var end_vec : Vector2 = mouse_position - spin["center"]
	spin["rotation"] = spin["start_vec"].angle_to(end_vec)
	
	state = states.SPIN
	var duration = spin_180_time * (abs(spin["rotation"]) / PI)
	$SpinTween.interpolate_method(self, "_update_spin", 0, 1.0, duration,
			Tween.TRANS_BACK, Tween.EASE_OUT)
	$SpinTween.start()



func _update_spin(progress: float) -> void:
	var amount : float = spin["rotation"] * progress
#	spin["spinner"].global_position = \
#			spin["center"] + spin["start_vec"].rotated(amount)
	var cur : Vector2 = spin["spinner"].global_position 
	var next : Vector2 = spin["center"] + spin["start_vec"].rotated(amount)
	var movement := next - cur
	spin["spinner"].spin_move(movement)


func char_has_spin_moved() -> void:
	_recenter_characters()
	_orient_characters()


func _orient_characters() -> void:
	var a_angle := (char_a.position - char_b.position).angle()
	char_a.update_direction(a_angle)
	char_b.update_direction(a_angle + PI)


func _recenter_characters() -> void:
	var vec : Vector2 = (char_b.position - char_a.position).normalized()
	var center_vec = vec * (link_length / 2.0)
	global_position = char_a.global_position + center_vec
	char_a.position = - center_vec
	char_b.position = char_a.position + (vec * link_length)


func end_spin(collision := false) -> void:
	$SpinTween.remove(self, "_update_spin")
	state = states.IDLE
	if collision:
		Effect.screen_shake(0.1, 2)


func _on_spin_tween_completed(object, key) -> void:
	end_spin()
