class_name Character
extends KinematicBody2D

var movement : Vector2

signal has_spin_moved
signal spin_collision

func update_direction(new_direction: float) -> void:
	$HandAxis.rotation = new_direction


func spin_move(vector: Vector2) -> void:
	movement = vector


func _physics_process(delta) -> void:
	if movement != Vector2.ZERO:
		var collision := move_and_collide(movement)
		movement = Vector2.ZERO
		emit_signal("has_spin_moved")
		if collision:
			emit_signal("spin_collision")
		
