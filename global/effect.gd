extends Node


signal screen_shake(duration, strength)

func _ready() -> void:
	randomize()


static func screen_freeze(duration: float) -> void:
	OS.delay_msec(duration * 1000) #converts from secs to msecs


func screen_shake(duration: float, strength: float):
	emit_signal("screen_shake", duration, strength)


func one_shot(pos: Vector2, anim: String, depth:= 0):
	var scene = preload("res://main/one_shot_effect.tscn").instance()
	get_tree().root.add_child(scene)
	print("OneShotEffect instanced in tree - NEEDS CHANGE")
	scene.global_position = pos
	scene.z_index = depth
	scene.set_animation(anim)
